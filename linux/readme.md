# Index

- [Fix hotspot wifi logins](#fix-hotspot-wifi-logins)
- [SSL config for nginx reverse proxy](#ssl-config-for-nginx-reverse-proxy)
- [Read CPU temperature](#read-cpu-temperature)
- [Resize tmux window pane](#resize-tmux-window-pane)
- [Create a systemd service](#create-a-systemd-service)
- [Delete lines in a file with sed](#delete-lines-in-a-file-with-sed)
- [Unmount and safely remove disk](#unmount-and-safely-remove-disk)
- [Format disk for new use](#format-disk-for-new-use)
- [Create a LUKS encrypted USB device partition](#create-a-luks-encrypted-usb-device-partition)
- [Open and close LUKS USB device](#open-and-close-luks-usb-device)
- [Curl with username and password](#curl-with-username-and-password)
- [VirtualBox fails to start with kernel driver not installed error](#virtualbox-fails-to-start-with-kernel-driver-not-installed-error)
- [curl and show HTTP code only](#curl-and-show-http-code-only)
- [SSH through jumpbox tunnel to protected server](#ssh-through-jumpbox-tunnel-to-protected-server)
- [Discover how a package was installed with dnf](#discover-how-a-package-was-installed-with-dnf)
- [Troubleshooting client DNS](#troubleshooting-client-dns)
- [Install Python 3 on CentOS](#install-python-3-on-centos)
- [Copy and paste in tmux](#copy-and-paste-in-tmux)
- [Find out which files are part of a package in DNF](#find-out-which-files-are-part-of-a-package-in-dnf)
- [Install an AppImage on Fedora](#install-an-appimage-on-fedora)
- [Get current public IP address](#get-current-public-ip-address)
- [Copy SSH keys to a new machine](#copy-ssh-keys-to-a-new-machine)
- [Fix and add an SELinux policy module](#fix-and-add-an-selinux-policy-module)
- [Locate Desktop Environment shortcut link GUI icon bin locations](#locate-desktop-environment-shortcut-link-gui-icon-bin-locations)
- [Get machine hardware information](#get-machine-hardware-information)

### Fix hotspot wifi logins

Issue and resolution originally found with Fedora 26.

Install the `dnssec-trigger-panel` package: `$ sudo dnf install -y dnssec-trigger-panel`

*Note: it appears as though running this panel throws an SELinux exception `execute_no_trans`. It looks like this can be safely ignored

### SSL config for nginx reverse proxy

This will configure SSL (the certificate from LetsEncrypt) termination on nginx

*Credit goes to [this nginx blog post on the topic](https://www.nginx.com/blog/free-certificates-lets-encrypt-and-nginx/) which guided me through this originally*

Clone certbot: `$ git clone https://github.com/certbot/certbot /opt/letsencrypt`

```
$ cd /var/www
$ mkdir letsencrypt
$ sudo chgrp www-data letsencrypt
```

Create `/etc/letsencrypt/configs/FQDN.conf` (where `FQDN` is replaced with its appropriate value for the desired domain name). Put the following contents in the file, replacing the `FULL CAPS` place holders:

```
domains = FQDN
rsa-key-size = 2048
server = https://acme-v01.api.letsencrypt.org/directory
email = EMAIL_ADDRESS_FOR_RENEWAL_NOTIFICATION
text = True
authenticator = webroot
webroot-path = /var/www/letsencrypt/
```

Add a location block pointing to the temp dir. This will be in `/etc/nginx/sites-enabled` (most likely symlinked to an available site config). Add this location block to the root of the server block (mostly likely there are other location blocks there, just add it adjacent to them as in before):

```
server {
    ...
    location /.well-known/acme-challenge {
        root /var/www/letsencrypt;
    }
    ...
}
```

Restart nginx: `$ sudo nginx -s reload`

Request the certificate:

```
$ cd /opt/letsencrypt
$ ./certbot-auto --config /etc/letsencrypt/configs/FQDN.conf certonly
```

Alter the nginx site config to use HTTPS:

```
server {
    listen 443 ssl default_server;
    server_name FQDN;
    ssl_certificate /etc/letsencrypt/live/FQDN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/FQDN/privkey.pem;
}
```

*Note: replace FQDN with the fully qualified domain name of the target domain name*

Reload nginx: `$ sudo nginx -s reload`

### Read CPU temperature

```
$ sudo dnf install -y lm_sensors
$ sensors
```

This outputs a few important items:

1. Fan speed
1. Temp in CPU socket or nearby (acpitz-virtual-0)
1. CPU temperature
1. GPU temperature and power

### Resize tmux window pane

`ctrl-b + ctrl-<arrow_key>`

Keep pressing `ctrl-<arrow_key>` to continue to move it

### Create a systemd service

Add a unit file to `/etc/systemd/system` named `<service_name>.service` with the following contents:

```
[Unit]
Description=Service description
After=network.target

[Service]
Type=simple
User=youruser
ExecStart=/path/to/bin --args
Restart=on-abort

[Install]
WantedBy=multi-user.target
```

Reload systemd: `# systemctl daemon-reload`

Start the service: `# systemctl start <service_name>`

Enable the service (start automatically): `# systemctl enable <service_name>`

### Delete lines in a file with sed

```
$ sed -i '/regex pattern to match/d' ./file
```

### Unmount and safely remove disk

```
$ sudo udisksctl unmount -b /dev/sdc1
$ sudo udisksctl power-off -b /dev/sdc
```

*Note*: Substitute the property device for unmount and the proper disk for power-off

### Format disk for new use

If you need to delete an existing partition...

First list out the disks and partitions:

```
$ sudo fdisk -l
```

Then delete any partitions necessary:

```
$ sudo fdisk /dev/sdc
```

While in fdisk, run `d` to delete a partition. Then run `n` to create a new partition if necessary.

Create a filesystem from the new partition:

```
$ sudo mkfs.ext4 /dev/sdc1
```

Mount the new partition:

```
$ sudo mount /dev/sdc1 /mnt
```

### Create a LUKS encrypted USB device partition

If necessary, delete any partitions that are already on the drive:

```
$ sudo fdisk /dev/sdc
```

Run `d` to delete partitions, and `n` to create a new one. `w` to save and exit.

Setup the LUKS partition:

```
$ sudo cryptsetup -yv luksFormat /dev/sdc1
```

Open the LUKS partition:

```
$ sudo cryptsetup luksOpen /dev/sdc1 hddext
```

Format the partition:

```
$ sudo mkfs.ext4 /dev/mapper/hddext
```

Mount the partition:

```
$ sudo mount /dev/mapper/hddext /mnt
```

Or use udisks to mount in the default location:

```
$ sudo udisksctl mount -b /dev/mapper/hddext
```

### Open and close LUKS USB device

First attempt to insert the USB device, Fedora/GNOME prompt to unlock it. If that doesn't work run: `# cryptsetup luksOpen /dev/sdc1 extdisk`.

Find out the name by running `$ lsblk`.

Mount the mapped device: `$ sudo mount /dev/mapper/<dev_name> /<mount_point>`

Or use udisks: `$ udisksctl mount -b /dev/mapper/<dev_name>`

To close: `$ udisksctl unmount -b /dev/mapper/<dev_name>` 

Lock the device: `$ sudo cryptsetup luksClose /dev/mapper/<dev_name>`

Power off the device: `$ udisksctl power-off -b /dev/sdc`

### Curl with username and password

```
$ curl --user <username>:<password> <url>
```

### VirtualBox fails to start with kernel driver not installed error

This originally happened after I upgrade Fedora (and therefore my Linux kernel). Trying to start it from the terminal:

```
$ VirtualBox restart
```

I saw the error message saying I should do the following:

```
akmods --kernels 4.13.13-300.fc27.x86_64 && systemctl restart systemd-modules-load.service
sudo akmods --kernels 4.13.13-300.fc27.x86_64 && systemctl restart systemd-modules-load.service
```

_Note: the kernel versions will likely be different_

### curl and show HTTP code only

```
$ curl -s -o /dev/null -w "%{http_code}" <url>
```

### SSH through jumpbox tunnel to protected server

```
$ ssh -J username@jumpbox username@protectedserver
```

### Discover how a package was installed with dnf

```
# dnf history list <package_name>
```

This will list out the history entries for installation, update, etc.

To show the history details of an entry listed above: `# dnf history info <history_entry_id>`

### Troubleshooting client DNS

If it is believed that there is internet connectivity but name resolution is not working, first verify by pinging a known address: `$ ping 8.8.8.8`

Then ensure that name resolution isn't working: `$ nslookup google.com`

1. Look through `/etc/resolv.conf` to see what the configured nameservers are
1. Try to ping the nameservers
1. Look through the log for NetworkManager to see nameserver issues: `# journalctl -b -u NetworkManager`

If the DNS server is not working (can happen in certain public spots) then modify `/etc/resolv.conf` and make it so that the only entry is `nameserver 8.8.8.8` (that's the Google DNS server, consider using OpenDNS servers as well)

To troubleshoot possible issues, run `$ dig google.com` to see if the tool dumps any errors

### Install Python 3 on CentOS

```
# yum -y update
# yum -y install yum-utils
# yum -y groupinstall development
# yum -y install https://centos7.iuscommunity.org/ius-release.rpm
# yum -y install python36u
# yum -y install python36u-pip
# yum -y install python36u-devel
```

### Copy and paste in tmux

- Go into select mode: `CTRL+b [`
- Navigate to the text with normal nav keys
- Hit `SPACE` to go into selection
- Navigate to the end of the text in selection
- Hit `ENTER` to copy selection to clipboard

Paste by doing `CTRL+b ]`

### Find out which files are part of a package in DNF

```
$ dnf repoquery -l <package_name>
```

### Install an AppImage on Fedora

Make the AppImage file executable and then run it:

```
$ chmod 755 <AppImage_file>
$ ./<AppImage_file>
```

### Get current public IP address

```
$ curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'
```

### Copy SSH keys to a new machine

You can copy the private (and public) SSH keys to a new machine. After this is done, you must run `ssh-add` on the new machine.

### Fix and add an SELinux policy module

To consume and generate policy rules from logs run the following:

```
$ # dump the logs (from journalctl?) to a flat file
$ audit2allow -i <log_dump>
```

That shows the new policies we want to use. Then generate the module:

```
$ audit2allow -i <log_dump> -M <new_module_name>
```

The output will be the command you need to run with `semanage` to load the module.

### Locate Desktop Environment shortcut link GUI icon bin locations

```
$ cd /usr/share/applications
$ grep -r "<regex_search_string>" *.desktop
$ grep "Exec=" <found_filename_from_above>
```

### Get machine hardware information

```
$ dmidecode
```

This includes info like manufacturing and model number.
