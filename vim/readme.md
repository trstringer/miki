# Index

- [Search through all files](#search-through-all-files)
- [Quickfix location preview windows](#quickfix-location-preview-windows)
- [Copy selected lines to clipboard](#copy-selected-lines-to-clipboard)
- [Count occurrences of regex string in current file](#count-occurrences-of-regex-string-in-current-file)
- [Pipe stdout to Vim](#pipe-stdout-to-vim)
- [Resize window](#resize-window)
- [YouCompleteMe fails on libtinfo](#youcompleteme-fails-on-libtinfo)
- [Bad Vim colors while running in tmux](#bad-vim-colors-while-running-in-tmux)
- [Syntastic pylint does not recognize Python virtual environment](#syntastic-pylint-does-not-recognize-python-virtual-environment)
- [Include header files not on the default path for YouCompleteMe](#include-header-files-not-on-the-default-path-for-youcompleteme)
- [Delete all lines that match](#delete-all-lines-that-match)
- [Echo and show variable value](#echo-and-show-variable-value)
- [Search through files that contains or omits multiple strings](#search-through-files-that-contains-or-omits-multiple-strings)
- [List and open files by matching their name](#list-and-open-files-by-matching-their-name)
- [Open up a new blank buffer window pane](#open-up-a new-blank-buffer-window-pane)
- [Open up a new blank buffer tab](#open-up-a-new-blank-buffer-tab)
- [Copy the full path and filename of the buffer to the clipboard](#copy-the-full-path-and-filename-of-the-buffer-to-the-clipboard)

### Search through all files

`:vim /regex/ **/*`

### Quickfix location preview windows

**Quickfix** - open with `:cope`, close with `:ccl` (across multiple files)

**Location** - open with `:lop`, close with `:lcl` (current window info)

### Copy selected lines to clipboard

Select the lines to copy in visual module.

In visual mode run `:w ! xclip -sel c`

*Note: you need to ensure that xclip is installed*

### Count occurrences of regex string in current file

`:%s/regex//gn`

### Pipe stdout to Vim

`$ echo 'hello world' | vim -`

### Resize window

- `ctrl-w 10 +` makes the window taller
- `ctrl-w 10 -` makes the window shorter
- `ctrl-w 10 >` makes the window wider
- `ctrl-w 10 <` makes the window thinner

*Note: `10` can be replaced with any value*

### YouCompleteMe fails on libtinfo

When opening up Vim with YCM enabled, there was an error that libtinfo.so.5 couldn't be found.

On Fedora 27, the fix was to install the `ncurses-compat-libs` package:

```
$ sudo dnf install ncurses-compat-libs -y
```

### Bad Vim colors while running in tmux

The colors were off with Vim in tmux, but not out of tmux. It turned out that Vim had background (`:echo &background`) set to `light` while in tmux and `dark` outside of tmux.

The fix was to add the following to `.vimrc`: `set background=dark`

### Syntastic pylint does not recognize Python virtual environment

This is because pylint is not installed in the virtual environment, causing import-error issues.

The solution is to run `$ . venv/bin/activate && pip install pylint`

### Include header files not on the default path for YouCompleteMe

Modify `~/.ycm_extra_conf.py` to include `'-I', 'include'` in the flags list:

```python
def FlagsForFile(filename, **kwargs):
    return {
        'flags': [
            '-x',
            'c',
            '-Wall',
            '-Wextra',
            '-Werror',
            '-I', 'include'
        ],
    }
```

### Delete all lines that match

```
:g/<regex>/normal dd
```

### Echo and show variable value

```
:echo &variablename
```

Or

```
:set variablename?
```

### Search through files that contains or omits multiple strings

Search for lines that include two strings:

```
:LogiPat "include" & "moreinclude"
```

Search for lines that includes a string but not another:

```
:LogiPat "include" & !"exclude"
```

### List and open files by matching their name

In your .vimrc you should have similar settings:

```
set wildignore+=**/vendor/**
set wildmenu
set wildmode=list:full
set path=.,,**
```

In Vim in normal mode, type `:find <search_string>` and then `TAB` to list all files and scroll through them (hitting `ENTER` when you are on the file to open and edit)

### Open up a new blank buffer window pane

- New pane split horizontally: `:new`
- New pane split vertically: `:vnew`

### Open up a new blank buffer tab

- New tab after the current one: `:tabe`
- New tab at the end of all tabs: `:$tabe`

### Copy the full path and filename of the buffer to the clipboard

```
:!echo -n %:p | xclip -sel c
```
