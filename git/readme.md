# Index

- [Ignore changes to a specific file](#ignore-changes-to-a-specific-file)
- [Set local branch to track remote branch](#set-local-branch-to-track-remote-branch)
- [Squash commits](#squash-commits)
- [Find git configuration origin](#find-git-configuration-origin)
- [Set global gitignore](#set-global-gitignore)
- [Move git hooks to a version-controlled dir](#move-git-hooks-to-a-version-controlled-dir)
- [Add a release on GitHub with curl](#add-a-release-on-github-with-curl)
- [Get if current commit is also a tag](#get-if-current-commit-is-also-a-tag)

### Ignore changes to a specific file

`$ git update-index --assume-unchanged <file_name>`

### Set local branch to track remote branch

Explicitly:

```
$ git branch -u <remote>/<branch>
```

Or on push:

```
$ git push -u <remote>/<branch>
```

### Squash commits

Either do a soft reset followed by a new commit:

```
$ git reset --soft <commit_sha>
$ git commit -m <commit_message>
```

Or do an interactive rebase:

```
$ git rebase -i <commit_sha>
```

### Find git configuration origin

To find out where each configuration item came from:

```
$ git config --list --show-origin
```

### Set global gitignore

Create a global `.gitignore` file (e.g. `~/.gitignore`) and then run:

```
$ git config --global core.excludesFile ~/.gitignore
```

### Move git hooks to a version-controlled dir

It is possible to move git hooks outside of the `.git/hooks` dir so they too can be versioned.

```
$ cd <git_repo>
$ mkdir .git_hooks
$ # place git hooks in ./.git_hooks
$ git config core.hooksPath ./.git_hooks
```

### Add a release on GitHub with curl

```
$ curl -L -H "Authorization: token <personal_access_token>" -H "Content-Type: application/json" --request POST --data '{"tag_name": "<target_tag_name>", "name": "<release_name>", "body": "<release_body>"}' https://api.github.com/repos/<owner>/<repo>/releases
```

### Get if current commit is also a tag

```
$ git describe --exact-match --tags $(git log -n1 --pretty='%h')
```
