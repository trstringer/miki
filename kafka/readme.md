# Index

- [Listen on public DNS IP address](#listen-on-public-dns-ip-address)

### Listen on public DNS IP address

To listen on an IP address or host name that isn't bound on the local machine, do the following:

```
listeners=CLIENT://:9090,PLAINTEXT://:9092
advertised.listeners=CLIENT://kafkavm1.eastus.cloudapp.azure.com:9090,PLAINTEXT://:9092
```
