# Index

- [Run a playbook on localhost](#run-a-playbook-on-localhost)

### Run a playbook on localhost

Set the play key `hosts` to `localhost`.

```yml
---
- name: my play
  hosts: localhost

  tasks:
    ...
```
