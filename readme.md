# Index

- [ansible](#ansible)
- [aws](#aws)
- [azure](#azure)
- [bash](#bash)
- [docker](#docker)
- [fluentd](#fluentd)
- [git](#git)
- [go](#go)
- [jenkins](#jenkins)
- [kafka](#kafka)
- [kubernetes](#kubernetes)
- [linux](#linux)
- [linux_programming](#linux_programming)
- [postgres](#postgres)
- [python](#python)
- [vim](#vim)

## ansible 

- [Run a playbook on localhost](ansible#run-a-playbook-on-localhost)

## aws 

- [Login to the aws cli with IAM](aws#login-to-the-aws-cli-with-iam)
- [Retrieve the Kubernetes config file for kubectl from a cluster](aws#retrieve-the-kubernetes-config-file-for-kubectl-from-a-cluster)

## azure 

- [Create a Linux VM from the cli](azure#create-a-linux-vm-from-the-cli)
- [List public IPs and their FQDNs](azure#list-public-ips-and-their-fqdns)
- [Deploy a Kubernetes cluster with acs-engine](azure#deploy-a-kubernetes-cluster-with-acs-engine)
- [Create a postgres server](azure#create-a-postgres-server)
- [List VM power states](azure#list-vm-power-states)
- [Create a network security group (nsg) rule to allow traffic](azure#create-a-network-security-group-nsg-rule-to-allow-traffic)
- [Create a Key Vault and store a secret in it](azure#create-a-key-vault-and-store-a-secret-in-it)
- [Create an Azure Container Registry](azure#create-an-azure-container-registry)

## bash 

- [Get the directory of the running shell script](bash#get-the-directory-of-the-running-shell-script)
- [Move everything from a dir into a sub dir](bash#move-everything-from-a-dir-into-a-sub-dir)
- [Print from the terminal with lpr](bash#print-from-the-terminal-with-lpr)
- [Split a string on a character](bash#split-a-string-on-a-character)

## docker 

- [Run a container and bind a port](docker#run-a-container-and-bind-a-port)
- [Automatically remove container after running](docker#automatically-remove-container-after-running)
- [Pass environment variables to container](docker#pass-environment-variables-to-container)
- [Pass arguments to container](docker#pass-arguments-to-container)
- [Sample Docker Compose configuration for exposing ports](docker#sample-docker-compose-configuration-for-exposing-ports)
- [Sync container time with host time](docker#sync-container-time-with-host-time)

## fluentd 

- [Build Fluent Bit on CentOS](fluentd#build-fluent-bit-on-centos)
- [Build Fluent Bit on CentOS with systemd support](fluentd#build-fluent-bit-on-centos-with-systemd-support)
- [Create a new output plugin for Fluent Bit](fluentd#create-a-new-output-plugin-for-fluent-bit)

## git 

- [Ignore changes to a specific file](git#ignore-changes-to-a-specific-file)
- [Set local branch to track remote branch](git#set-local-branch-to-track-remote-branch)
- [Squash commits](git#squash-commits)
- [Find git configuration origin](git#find-git-configuration-origin)
- [Set global gitignore](git#set-global-gitignore)
- [Move git hooks to a version-controlled dir](git#move-git-hooks-to-a-version-controlled-dir)
- [Add a release on GitHub with curl](git#add-a-release-on-github-with-curl)
- [Get if current commit is also a tag](git#get-if-current-commit-is-also-a-tag)

## go 

- [Create and work with slices](go#create-and-work-with-slices)
- [Create and work with maps](go#create-and-work-with-maps)
- [Create and work with structs](go#create-and-work-with-structs)
- [Creating and using basic goroutines and channels](go#creating-and-using-basic-goroutines-and-channels)
- [Run a command and get the output](go#run-a-command-and-get-the-output)

## jenkins 

- [GitHub webhook](jenkins#github-webhook)
- [Configure SSL for Jenkins nginx reverse proxy (redirect to Linux)](../linuxjenkins#ssl-config-for-nginx-reverse-proxy)
- [Run docker container in pipeline](jenkins#run-docker-container-in-pipeline)
- [Pass secrets to pipeline](jenkins#pass-secrets-to-pipeline)

## kafka 

- [Listen on public DNS IP address](kafka#listen-on-public-dns-ip-address)

## kubernetes 

- [Dynamically get pod logs](kubernetes#dynamically-get-pod-logs)
- [Deploy a Kubernetes cluster with acs-engine (mikird)](kubernetes#deploy-a-kubernetes-cluster-with-acs-engine-mikird)
- [Change API server configuration and options](kubernetes#change-api-server-configuration-and-options)
- [Enable initializers for the cluster](kubernetes#enable-initializers-for-the-cluster)
- [SSH to agent nodes in Azure](kubernetes#ssh-to-agent-nodes-in-azure)
- [List all helm charts and their versions](kubernetes#list-all-helm-charts-and-their-versions)
- [Restart a pod](kubernetes#restart-a-pod)
- [List pods and display only names](kubernetes#list-pods-and-display-only-names)
- [Create a PersistentVolumeClaim with Azure Files](kubernetes#create-a-persistentvolumeclaim-with-azure-files)
- [Pull Helm releases ConfigMaps](kubernetes#pull-helm-releases-configmaps)
- [Create a second Tiller](kubernetes#create-a-second-tiller)
- [Requirements for Istio installation](kubernetes#requirements-for-istio-installation)
- [Discover what manifests would be installed by Helm without creating resources](kubernetes#discover-what-manifests-would-be-installed-by-helm-without-creating-resources)
- [Add or change a resource label](kubernetes#add-or-change-a-resource-label)
- [Set and use Azure Active Directory for authentication with Kubernetes role based access control RBAC](kubernetes#set-and-use-azure-active-directory-for-authentication-with-kubernetes-role-based-access-control-rbac)
- [Use jsonpath to extract fields from a list of output](kubernetes#use-jsonpath-to-extract-fields-from-a-list-of-output)

## linux 

- [Fix hotspot wifi logins](linux#fix-hotspot-wifi-logins)
- [SSL config for nginx reverse proxy](linux#ssl-config-for-nginx-reverse-proxy)
- [Read CPU temperature](linux#read-cpu-temperature)
- [Resize tmux window pane](linux#resize-tmux-window-pane)
- [Create a systemd service](linux#create-a-systemd-service)
- [Delete lines in a file with sed](linux#delete-lines-in-a-file-with-sed)
- [Unmount and safely remove disk](linux#unmount-and-safely-remove-disk)
- [Format disk for new use](linux#format-disk-for-new-use)
- [Create a LUKS encrypted USB device partition](linux#create-a-luks-encrypted-usb-device-partition)
- [Open and close LUKS USB device](linux#open-and-close-luks-usb-device)
- [Curl with username and password](linux#curl-with-username-and-password)
- [VirtualBox fails to start with kernel driver not installed error](linux#virtualbox-fails-to-start-with-kernel-driver-not-installed-error)
- [curl and show HTTP code only](linux#curl-and-show-http-code-only)
- [SSH through jumpbox tunnel to protected server](linux#ssh-through-jumpbox-tunnel-to-protected-server)
- [Discover how a package was installed with dnf](linux#discover-how-a-package-was-installed-with-dnf)
- [Troubleshooting client DNS](linux#troubleshooting-client-dns)
- [Install Python 3 on CentOS](linux#install-python-3-on-centos)
- [Copy and paste in tmux](linux#copy-and-paste-in-tmux)
- [Find out which files are part of a package in DNF](linux#find-out-which-files-are-part-of-a-package-in-dnf)
- [Install an AppImage on Fedora](linux#install-an-appimage-on-fedora)
- [Get current public IP address](linux#get-current-public-ip-address)
- [Copy SSH keys to a new machine](linux#copy-ssh-keys-to-a-new-machine)
- [Fix and add an SELinux policy module](linux#fix-and-add-an-selinux-policy-module)
- [Locate Desktop Environment shortcut link GUI icon bin locations](linux#locate-desktop-environment-shortcut-link-gui-icon-bin-locations)
- [Get machine hardware information](linux#get-machine-hardware-information)

## linux_programming 

- [Simple Makefile for dependency compilation](linux_programming#simple-makefile-for-dependency-compilation)
- [Make an HTTP request with libcurl](linux_programming#make-an-http-request-with-libcurl)
- [Simple Makefile for compiling against libcurl](linux_programming#simple-makefile-for-compiling-against-libcurl)
- [Creating a C library and linking it from another bin](linux_programming#creating-a-c-library-and-linking-it-from-another-bin)
- [List all paths for that gcc will look through for linking by default](linux_programming#list-all-paths-for-that-gcc-will-look-through-for-linking-by-default)
- [Add a path for linking a shared object file when running a bin](linux_programming#add-a-path-for-linking-a-shared-object-file-when-running-a-bin)
- [Compile a bin that can be debugged from gdb](linux_programming#compile-a-bin-that-can-be-debugged-from-gdb)
- [Show the default paths gcc with search for include header files](linux_programming#show-the-default-paths-gcc-with-search-for-include-header-files)

## postgres 

- [Connect to different database in psql](postgres#connect-to-different-database-in-psql)
- [Create a postgres server in azure (mikird)](postgres#create-a-postgres-server-in-azure-mikird)

## python 

- [Install and activate a virtual environment](python#install-and-activate-a-virtual-environment)
- [Create a basic Flask application](python#create-a-basic-flask-application)
- [Docker configuration for a Flask application](python#docker-configuration-for-a-flask-application)
- [Debugging a Flask application from the command line](python#debugging-a-flask-application-from-the-command-line)
- [Use Google authentication for a Flask application](python#use-google-authentication-for-a-flask-application)
- [Debug a Python or Flask application in a container with Docker Compose](python#debug-a-python-or-flask-application-in-a-container-with-docker-compose)
- [Flask application logging with Gunicorn](python#flask-application-logging-with-gunicorn)
- [Log to systemd journal](python#log-to-systemd-journal)
- [Specify a different Pipfile with pipenv](python#specify-a-different-pipefile-with-pipenv)
- [Setup pipenv virtual environment](python#setup-pipenv-virtual-environment)
- [Create a pipenv script](python#create-a-pipenv-script)

## vim 

- [Search through all files](vim#search-through-all-files)
- [Quickfix location preview windows](vim#quickfix-location-preview-windows)
- [Copy selected lines to clipboard](vim#copy-selected-lines-to-clipboard)
- [Count occurrences of regex string in current file](vim#count-occurrences-of-regex-string-in-current-file)
- [Pipe stdout to Vim](vim#pipe-stdout-to-vim)
- [Resize window](vim#resize-window)
- [YouCompleteMe fails on libtinfo](vim#youcompleteme-fails-on-libtinfo)
- [Bad Vim colors while running in tmux](vim#bad-vim-colors-while-running-in-tmux)
- [Syntastic pylint does not recognize Python virtual environment](vim#syntastic-pylint-does-not-recognize-python-virtual-environment)
- [Include header files not on the default path for YouCompleteMe](vim#include-header-files-not-on-the-default-path-for-youcompleteme)
- [Delete all lines that match](vim#delete-all-lines-that-match)
- [Echo and show variable value](vim#echo-and-show-variable-value)
- [Search through files that contains or omits multiple strings](vim#search-through-files-that-contains-or-omits-multiple-strings)
- [List and open files by matching their name](vim#list-and-open-files-by-matching-their-name)
- [Open up a new blank buffer window pane](vim#open-up-a new-blank-buffer-window-pane)
- [Open up a new blank buffer tab](vim#open-up-a-new-blank-buffer-tab)
- [Copy the full path and filename of the buffer to the clipboard](vim#copy-the-full-path-and-filename-of-the-buffer-to-the-clipboard)