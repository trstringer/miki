# Index

- [Run a container and bind a port](#run-a-container-and-bind-a-port)
- [Automatically remove container after running](#automatically-remove-container-after-running)
- [Pass environment variables to container](#pass-environment-variables-to-container)
- [Pass arguments to container](#pass-arguments-to-container)
- [Sample Docker Compose configuration for exposing ports](#sample-docker-compose-configuration-for-exposing-ports)
- [Sync container time with host time](#sync-container-time-with-host-time)

### Run a container and bind a port

```
$ docker run -d -p 8000:8000 --name container_name image_name
```

### Automatically remove container after running

Use the `--rm` parameter when calling `docker run`.

```
$ docker run -d -p 8000:8000 --rm --name container_name imagine_name
```

### Pass environment variables to container

Either do this with `docker create` or `docker run`.

```
$ docker [create|run] ... -e KEY=VALUE
```

Or do this with an env file (more secure, recommended).

```
$ docker [create|run] ... --env-file env
```

Where `env` file is in the format of `KEY=VALUE` on each line.

### Pass arguments to container

In the case you want to run a container as a cli specify the following in the `Dockerfile`:

```
...
ENTRYPOINT ["passed", "args"]
CMD ["default", "args"]
```

In the above case `CMD` is optional in case you want default arguments. The `ENTRYPOINT` arguments are passed through to the container.

To run this container with args: `$ docker run --name mycli myimage my args here`. If it is already created, you can start it and tail the logs `$ docker logs -f mycli`. Or you can start the container interactively `$ docker start -i mycli`.

### Sample Docker Compose configuration for exposing ports

```
version: '3'
services:
  svc1:
    build: ./svc1/
    ports:
      - "8080:8000"
  svc2:
    build: ./svc2/
    ports:
      - "8081:8000"
```

### Sync container time with host time

```
$ docker run ... -v /etc/localtime:/etc/localtime:ro
```
