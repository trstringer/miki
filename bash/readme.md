# Index

- [Get the directory of the running shell script](#get-the-directory-of-the-running-shell-script)
- [Move everything from a dir into a sub dir](#move-everything-from-a-dir-into-a-sub-dir)
- [Print from the terminal with lpr](#print-from-the-terminal-with-lpr)
- [Split a string on a character](#split-a-string-on-a-character)

### Get the directory of the running shell script

```
SCRIPT_PATH=$(dirname "$(realpath "$0")")
```

### Move everything from a dir into a sub dir

```
$ mv !(sub_dir) sub_dir
```

This moves everything in the current directly that's *not* `sub_dir`, into `sub_dir` (essentially archiving everything into the directory)

### Print from the terminal with lpr

List all printers (default as well as the names):

```
$ lpstat -d -p
```

Print a file to the default printer:

```
$ lpr <file>
```

### Split a string on a character

Set the IFS (internal field separater) just for the read command to parse:

```bash
INPUT="hello;world"
IFS=';' read -ra PARSED <<< "$INPUT"
echo "${PARSED[0]}"  # hello
echo "${PARSED[1]}"  # world
```
