# Index

- [Login to the aws cli with IAM](#login-to-the-aws-cli-with-iam)
- [Retrieve the Kubernetes config file for kubectl from a cluster](#retrieve-the-kubernetes-config-file-for-kubectl-from-a-cluster)

### Login to the aws cli with IAM

1. Login to the AWS IAM console: https://console.aws.amazon.com/iam
1. Navigate to *Users*
1. Select your username
1. Select *Security credentials* tab
1. Create a new access key
1. Login to the aws cli and create a profile: `$ aws configure`
1. When prompted for the key name and secret, input from above

### Retrieve the Kubernetes config file for kubectl from a cluster

`$ aws eks update-kubeconfig --name <k8s_cluster_name> --no-verify-ssl`
