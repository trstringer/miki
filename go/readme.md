# Index

- [Create and work with slices](#create-and-work-with-slices)
- [Create and work with maps](#create-and-work-with-maps)
- [Create and work with structs](#create-and-work-with-structs)
- [Creating and using basic goroutines and channels](#creating-and-using-basic-goroutines-and-channels)
- [Run a command and get the output](#run-a-command-and-get-the-output)

### Create and work with slices

```go
slice1 := []int{}
slice1 = append(slice1, 13)
slice1 = append(slice1, 86)
```

### Create and work with maps

```go
map1 := map[string]int{}
map1["hello"] = 3
map1["bye"] = 7
```

### Create and work with structs

```go
type Pato struct {
    Age  int
    Name string
}

func (p Pato) Quack() {
    fmt.Printf("Quack quack quack! My name is %s\n", p.Name)
}

func main() {
    quackers := Pato{13, "Quackers"}
    fmt.Printf("Pato's age is %d and name is %s\n", quackers.Age, quackers.Name)

    waddles := Pato{Name: "Waddles", Age: 15}
    fmt.Printf("Pato's age is %d and name is %s\n", waddles.Age, waddles.Name)

    waddles.Quack()
}
```

### Creating and using basic goroutines and channels

```go
package main

import "fmt"

func multiplyByHundred(in <-chan int, out chan<- int) {
	for start := range in {
		out <- start * 100
	}
	close(out)
}

func seed(in chan<- int) {
	for i := 0; i < 10; i++ {
		in <- i
	}
	close(in)
}

func main() {
	input := make(chan int)
	output := make(chan int)

	go seed(input)
	go multiplyByHundred(input, output)

	for end := range output {
		fmt.Printf("Output: %d\n", end)
	}
}
```

### Run a command and get the output

```go
import "os/exec"

cmd := exec.Command("echo", "-n", "hello world")
output, err := cmd.Output()
```
