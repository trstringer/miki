#!/bin/bash

# the first and only parameter to this script is an _optional_ bin name
# if not specified, it will default to "miki"
#
# usage:
#   ./install.sh othermikiname

CWD=$(python -c "import os; print(os.getcwd())")
BIN_NAME="miki"
if [[ -n "$1" ]]; then
    BIN_NAME="$1"
fi

python3 -m venv venv
# shellcheck disable=SC1091
. venv/bin/activate
pip install -r requirements.txt

cat > "$HOME/bin/$BIN_NAME" << EOF
cd $CWD
. venv/bin/activate
python miki.py "\$@"
EOF

chmod 755 "$HOME/bin/$BIN_NAME"
