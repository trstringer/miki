# Index

- [Simple Makefile for dependency compilation](#simple-makefile-for-dependency-compilation)
- [Make an HTTP request with libcurl](#make-an-http-request-with-libcurl)
- [Simple Makefile for compiling against libcurl](#simple-makefile-for-compiling-against-libcurl)
- [Creating a C library and linking it from another bin](#creating-a-c-library-and-linking-it-from-another-bin)
- [List all paths for that gcc will look through for linking by default](#list-all-paths-for-that-gcc-will-look-through-for-linking-by-default)
- [Add a path for linking a shared object file when running a bin](#add-a-path-for-linking-a-shared-object-file-when-running-a-bin)
- [Compile a bin that can be debugged from gdb](#compile-a-bin-that-can-be-debugged-from-gdb)
- [Show the default paths gcc with search for include header files](#show-the-default-paths-gcc-with-search-for-include-header-files)

### Simple Makefile for dependency compilation

```
CC=gcc
DEPS=other.h
OBJ=main.o other.o

%.o: %.c $(DEPS)
    $(CC) -c -o $@ $<

outputbin: $(OBJ)
    $(CC) -o $@ $^
```

### Make an HTTP request with libcurl

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

size_t write_callback(void *buffer, size_t size,
        size_t nmemb, void *userdata)
{
    printf("...... Received data in write_callback...\n");
    printf("...... Buffer received: %s\n", buffer);
    printf("...... userdata: %s\n", userdata);
    strcat(userdata, buffer);
    return size * nmemb;
}

int main()
{
    /* initialize curl */
    curl_global_init(CURL_GLOBAL_ALL);

    /* obtain a curl handle to use for setting options and making a request */
    CURL *curl_handle = curl_easy_init();
    if (!curl_handle)
    {
        printf("curl handle unable to be obtained...\n");
        curl_global_cleanup();
        return EXIT_FAILURE;
    }

    /* set the url for curl to make a request to */
    char *url = "http://localhost:3000";
    printf("Setting curl url to '%s'\n", url);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);

    /* set curl write function to handle data returned */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_callback);

    /* cache the output in this buffer and concat in the handler */
    char output_buffer[1024] = "";
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, output_buffer);

    /* set the curl handle to cache a friendly error in the event it fails */
    char error_buffer[CURL_ERROR_SIZE] = "";
    curl_easy_setopt(curl_handle, CURLOPT_ERRORBUFFER, error_buffer);

    /* make the actual curl request */
    CURLcode res = curl_easy_perform(curl_handle);
    if (res != CURLE_OK)
    {
        printf("Request failed: %s\n", error_buffer);
        return EXIT_FAILURE;
    }

    printf("Request successful\n");
    printf("Output buffer: %s\n", output_buffer);

    /* required: clean up curl */
    curl_global_cleanup();

    return EXIT_SUCCESS;
}
```

### Simple Makefile for compiling against libcurl

```
CC = gcc
DEPS =
OBJ = main.o
LDLIBS = -lcurl
LDFLAGS = -L/usr/local/lib
INCLUDE = -I/usr/local/include

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $<

outputbin: $(OBJ)
	$(CC) -o $@ $^ $(LDLIBS) $(INCLUDE) $(LDFLAGS)
```

### Creating a C library and linking it from another bin

The library needs to be created and then placed (header file(s)) in `/usr/include/<lib_name>` and the shared object file placed in `/usr/lib64`

The following is a simple library (libmathstuff) and application (maththings) highlighting how to accomplish this:

```
libmathstuff
├── Makefile
└── src
    ├── mathstuff.c
    └── mathstuff.h
maththings
├── Makefile
└── src
    ├── main.c
    └── main.o

2 directories, 6 files
```

**libmathstuff**

_src/mathstuff.h_

```c
#ifndef _MATHSTUFF_H_
#define _MATHSTUFF_H_

int add_two_numbers(int, int);

#endif
```

_src/mathstuff.c_

```c
int add_two_numbers(int first_number, int second_number)
{
    /* adding a comment here */
    return first_number + second_number;
}
```

_Makefile_

```
CC=gcc

default: copy

libmathstuff.so: src/mathstuff.c
    $(CC) -shared -o libmathstuff.so src/mathstuff.c

copy: libmathstuff.so
    cp libmathstuff.so /usr/lib64/
    mkdir -p /usr/include/libmathstuff
    cp src/mathstuff.h /usr/include/libmathstuff

clean:
    rm *.so

.PHONY: clean
```

**maththings**

_src/main.c_

```c
#include <stdio.h>
#include <stdlib.h>
#include <libmathstuff/mathstuff.h>

int main()
{
    printf("More things here\n");
    printf("This is the beginning...\n");
    int result = add_two_numbers(2, 5);
    printf("Result of math things is %d\n", result);
    return EXIT_SUCCESS;
}
```

_Makefile_

```
CC=gcc

maththings: src/main.o
    $(CC) -o maththings src/main.o -lmathstuff

src/main.o: src/main.c
    $(CC) -c -o src/main.o src/main.c
```

When running `make` for the library, it'll create the shared object file and place it in `/usr/lib64`. It will also copy the header file of the library into `/usr/include/<lib_name>`

When running `make` for the application, it'll link the library in with the `-l` param (the library must be prefixed with "lib" on the filesystem, but you do not specify lib when linking it)

### List all paths for that gcc will look through for linking by default

```
$ gcc -print-search-dirs | sed '/^lib/b 1;d;:1;s,/[^/.][^/]*/\.\./,/,;t 1;s,:[^=]*=,:;,;s,;,;  ,g' | tr \; \012 | tr ':' '\n'
```

### Add a path for linking a shared object file when running a bin

```
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<additional_path> <bin_to_exec>
```

Example:

```
$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib my_bin
```

### Compile a bin that can be debugged from gdb

Use the `-g` parameter with gcc:

```
$ gcc -g -o <output_bin_name> <source>
```

Then debug:

```
$ gdb <output_bin_name>
```

### Show the default paths gcc with search for include header files

```
$ echo | gcc -E -Wp,-v -
```
