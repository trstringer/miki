# Index

- [Create a Linux VM from the cli](#create-a-linux-vm-from-the-cli)
- [List public IPs and their FQDNs](#list-public-ips-and-their-fqdns)
- [Deploy a Kubernetes cluster with acs-engine](#deploy-a-kubernetes-cluster-with-acs-engine)
- [Create a postgres server](#create-a-postgres-server)
- [List VM power states](#list-vm-power-states)
- [Create a network security group (nsg) rule to allow traffic](#create-a-network-security-group-nsg-rule-to-allow-traffic)
- [Create a Key Vault and store a secret in it](#create-a-key-vault-and-store-a-secret-in-it)
- [Create an Azure Container Registry](#create-an-azure-container-registry)

### Create a Linux VM from the cli

```
$ az group create -n linuxvm-rg -l eastus
$ az vm create \
    -g linuxvm-rg \
    -n linuxvm1 \
    --ssh-key-value ~/.ssh/id_rsa.pub \
    --admin-username trstringer \
    --public-ip-address-dns-name linuxvm1 \
    --image centos
```

### List public IPs and their FQDNs

```
$ az network public-ip list --query "[*].{name:name,resourceGroup:resourceGroup,fqdn:dnsSettings.fqdn,ip:ipAddress}" -o table
```

### Deploy a Kubernetes cluster with acs-engine

Make sure that `acs-engine` is installed first (curl the tarball from the latest release in the GitHub repo, extract it, and place it in a $PATH location).

Clone the Azure/acs-engine repo: `$ git clone https://github.com/azure/acs-engine.git`

Copy the `./examples/kubernetes.json` configuration file to a temp location. Modify the contents to reflect your configuration (most likely `properties.linuxProfile.adminUsername and ssh.publicKeys[*].keyData`).

Run the following command:

```
$ acs-engine deploy \
    --subscription-id <subscription_id> \
    --dns-prefix <dns_prefix> \
    --location eastus \
    --auto-suffix \
    --api-model kubernetes.json \
    --resource-group <resource_group_name>
```

*Note: this assumes the `kubernetes.json` config file is in the current directory*

Copy the remote master's k8s config file by running: `$ scp <admin_username>@<master_hostname>:~/.kube/config ~/.kube/config`

### Create a postgres server

```
$ az postgres server create \
    -g <resource_group_name> \
    -n <postgres_server_name> \
    --compute-units 50 \
    --ssl-enforcement enabled \
    --performance-tier basic \
    --storage-size 51200 \
    --version 9.6 \
    -u <admin_username> \
    -p <admin_password>
```

*Notes*:
- Compute units can be from 100 - 800 for Standard performance tier
- Compute units can be from 50 - 100 for Basic performance tier
- Storage size can be from 125 - 10,000 GB for Standard performance tier
- Storage size can be 50 - 1,050 GB for Basic performance tier
- Storage increments in 125 GB spaces
- Storage specified in MiB

After the postgres server is created, you must add a firewall rule:

```
$ az postgres server firewall-rule create \
    -g <resource_group_name> \
    -n <rule_name> \
    -s <server_name> \
    --start-ip-address 0.0.0.0 \
    --end-ip-address 255.255.255.255
```

Now you can connect with `psql` and run your `CREATE DATABASE` command.

### List VM power states

```
$ az vm list -d --query "[*].{resourceGroup:resourceGroup,name:name,powerState:powerState}" -o table
```

### Create a network security group (nsg) rule to allow traffic

To allow traffic to a port for a network security group, run the following:

```
$ az network nsg rule create \
    -g <resource_group_name> \
    --nsg-name <nsg_name> \
    -n <new_rule_name> \
    --destination-port-range <port_or_range> \
    --priority 500
```

*Note*: you can alter the priority number to something other than 500

### List load balancer inbound NAT rules

```
$ az network lb inbound-nat-rule list \
    -g <resource_group_name> \
    --lb-name <load_balancer_name> \
    --query "[*].{name:name,protocol:protocol,source:frontendPort,dest:backendPort}" \
    -o table
```

### Create a Key Vault and store a secret in it

Create the key vault:

```
$ az group create -l <location> -n <resource_group_name>
$ az keyvault create -n <key_vault_name> -g <resource_group_name>
```

Save a secret in the key vault:

```
$ az keyvault secret set --vault-name <vault_name> -n <secret_name> --value <secret_value>
```

Retrieve the secret value:

```
$ az keyvault secret download --vault-name <vault_name> -n <secret_name> -f azure_keyvault_tmp &&
    cat azure_keyvault_tmp &&
    rm azure_keyvault_tmp
```

### Create an Azure Container Registry

Create the container registry:

```
$ az acr create -n <cr_name> -g <resource_group_name> --sku <sku>
```

Where `sku` is Basic, Standard, or Premium.

Create an Azure AD Service Principal to use for automated login:

```
$ az ad sp create-for-rbac -n <sp_name>
```

Retrieve the service principal ID:

```
$ az ad sp show --id <appId> -o table
```

Where `appId` is retrieve from the output of `create-for-rbac`. Note the `ObjectId` retrieved from `az ad sp show`. Create the role assignment:

```
$ az role assignment create --role <role_name> --scope <resource_id> --assignee-object-id <sp_object_id>
```

Where `role_name` is the name of the role (i.e. Contributor, Owner, Reader). `<resource_id>` is the scope which is the fully qualified resource urn:

```
$ az acr show -n <cr_name> --query "id"
```

Log into the container registry:

```
$ az acr login -n <cr_name>
```
