# Index

- [Dynamically get pod logs](#dynamically-get-pod-logs)
- [Deploy a Kubernetes cluster with acs-engine (mikird)](#deploy-a-kubernetes-cluster-with-acs-engine-mikird)
- [Change API server configuration and options](#change-api-server-configuration-and-options)
- [Enable initializers for the cluster](#enable-initializers-for-the-cluster)
- [SSH to agent nodes in Azure](#ssh-to-agent-nodes-in-azure)
- [List all helm charts and their versions](#list-all-helm-charts-and-their-versions)
- [Restart a pod](#restart-a-pod)
- [List pods and display only names](#list-pods-and-display-only-names)
- [Create a PersistentVolumeClaim with Azure Files](#create-a-persistentvolumeclaim-with-azure-files)
- [Pull Helm releases ConfigMaps](#pull-helm-releases-configmaps)
- [Create a second Tiller](#create-a-second-tiller)
- [Requirements for Istio installation](#requirements-for-istio-installation)
- [Discover what manifests would be installed by Helm without creating resources](#discover-what-manifests-would-be-installed-by-helm-without-creating-resources)
- [Add or change a resource label](#add-or-change-a-resource-label)
- [Set and use Azure Active Directory for authentication with Kubernetes role based access control RBAC](#set-and-use-azure-active-directory-for-authentication-with-kubernetes-role-based-access-control-rbac)
- [Use jsonpath to extract fields from a list of output](#use-jsonpath-to-extract-fields-from-a-list-of-output)

### Dynamically get pod logs

Define the following shell function:

```
podprefixlogs () 
{ 
    POD_PREFIX="$2";
    POD_NS="$1";
    POD_NAME=$(kubectl get pod -n kube-system -o jsonpath={.items[*].metadata.name} | tr " " "\n" | grep "$POD_PREFIX" | head -n 1);
    kubectl logs -n $POD_NS $POD_NAME
}
```

Invoke the function: `$ podprefixlogs <namespace> <pod_prefix>`

### Deploy a Kubernetes cluster with acs-engine (mikird)

See the [Azure section](../azure#deploy-a-kubernetes-cluster-with-acs-engine)

### Change API server configuration and options

SSH into the master node(s) and modify the following file: `/etc/kubernetes/manifests/kube-apiserver.yaml`

### Enable initializers for the cluster

Modify `/etc/kubernetes/manifests/kube-apiserver.yaml`. Make the following changes:

- Add `Initializers` to `--admission-control`
- Add `--runtime-config=admissionregistration.k8s.io/v1alpha1` arg

### SSH to agent nodes in Azure

First, list all FQDNs for public IP addresses to know which DNS name to use:

```
$ az network public-ip list --query "[*].{name:name,resourceGroup:resourceGroup,fqdn:dnsSettings.fqdn}" -o table
```

Next, list all of the agent nodes in the Kubernetes cluster:

```
$ kubectl get no -l kubernetes.io/role=agent
```

Decide which agent node you want to SSH to, and then use `ssh -J`:

```
$ ssh -J <user_name>@<load_balancer_ip> <user_name>@<agent_node>
```

### List all helm charts and their versions

List charts that match a string:

```
$ helm search <chart_name>
```

List charts and their versions:

```
$ helm search --versions <chart_name>
```

List charts with semver searching

```
$ helm search --versions --version=<sem_ver> <chart_name>
```

### Restart a pod

```
$ kubectl get po <pod_name> [-n <namespace_name>] -o yaml | kubectl replace --force -f -
```

### List pods and display only names

```
$ PODS=$(kubectl get po --show-all -o jsonpath="{.items..metadata.name}")
```

Optionally loop through the pods and do something:

```
$ for POD in $PODS; do echo $POD; done
```

### Create a PersistentVolumeClaim with Azure Files

Create a storage account in the same resource group as the k8s cluster:

`$ az storage account create -g <resource_group_name> -n <new_storage_account_name> --sku Standard_LRS -l <location>`

Create a new role to assign principals to allow them to create secrets:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: secret-creator
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["create"]
```

Create a new role binding to bind that role to user `system:serviceaccount:kube-system:persistent-volume-binder`:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  namespace: default
  name: secret-creator-binding
subjects:
  - kind: User
    name: system:serviceaccount:kube-system:persistent-volume-binder
system:serviceaccount:kube-system:persistent-volume-binder
    apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: secret-creator
  apiGroup: rbac.authorization.k8s.io
```

Create the PersistentVolumeClaim:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: azurestoragefile
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: azurefile
  resources:
    requests:
      storage: 5Gi
```

Create resources that consume the PersistentVolumeClaim:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: filepod2
spec:
  containers:
    - name: first
      image: centos:latest
      command: ["sleep", "50000"]
      volumeMounts:
        - name: azstoragefile
          mountPath: /etc/testing
  volumes:
    - name: azstoragefile
      persistentVolumeClaim:
        claimName: azurestoragefile
```

List out file shares in the Azure storage account to verify the share was created:

```
$ az storage share list --connection-string $(az storage account show-connection-string -g <resource_group_name> -n <storage_account_name> --query "connectionString") -o table
```

List out the files in a file share:

```
$ az storage file list --share-name <share_name> --connection-string $(az storage account show-connection-string -g <resource_group_name> -n <storage_account_name> --query "connectionString") -o table
```

### Pull Helm releases ConfigMaps

Get all config maps:

```
$ kubectl get cm --all-namespaces
```

The format of the Helm releases' config maps are `<release_name>.<release_version>` (e.g. `my_app.v1`). To get the config map in plain text:

```
$ kubectl get cm -n kube-system <configmap_name> -o jsonpath="{.data.release}" | base64 -d | gunzip -
```

### Create a second Tiller

```
$ helm init --tiller-namespace <non_kube_system_ns>
```

In Helm v2 you should not create a new Tiller in a namespace that already has Tiller. This is *not* for HADR of Tiller

### Requirements for Istio installation

The following admission controls need to be added to `/etc/kubernetes/manifests/kube-apiserver.yml`:

- MutatingAdmissionWebhook
- ValidatingAdmissionWebhook

Note: for acs-engine this should be placed in the api-model.json config:

```json
{
    ...
    "properties": {
        ...
        "orchestratorProfile": {
            ...
            "kubernetesConfig": {
                "apiServerConfig": {
                    "--admission-control": "NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,DenyEscalatingExec,AlwaysPullImages"
                }
            }
        }
    }
}
```

### Discover what manifests would be installed by Helm without creating resources

Run `helm install` by specifying both the `--dry-run` and `--debug` parameters:

```
$ helm install --dry-run --debug <chart_name>
```

### Add or change a resource label

```
$ kubectl label <resource_type> <resource_name> [--overwrite]
```

If the label already exists, you must specify `--overwrite`

### Set and use Azure Active Directory for authentication with Kubernetes role based access control RBAC

Create an acs-engine apimodel with the following section under `properties`:

```json
    "aadProfile": {
      "serverAppID": "...",
      "clientAppID": "...",
      "tenantID": "..."
    }
```

- `serverAppID` is the application ID for the server component for auth
    - Create a new AAD app registration
    - Type is web
    - URL does not matter but cannot be blank
    - Edit the manifest after creation to have `groupMembershipClaims` set to `All` (and save)
- `clientAppID` is the application ID for the client component (kubectl usually) for auth
    - Create a new AAD app registration
    - Type is native
    - Redirect URL does not matter
    - Delegate permissions to the AAD server application
        - Go to AAD client app settings
        - Click required permissions
        - Select add and search for the AAD server application
        - Select delegated permissions and then "Access <server_app_name>"
        - Select ok and then set/create permissions (next to the add button in the portal)
- `tenantID` is the AAD tenant ID (can be retrieved from `az account show`)

After the Kubernetes cluster is successfully deployed, you need to authenticate and add your AAD user:

```
$ cp _output/<deployment>/kubeconfig/kubeconfig.<location>.json ~/.kube/config
```

Now run a generic kubectl command to log in your AAD with device login:

```
$ kubectl get no
```

You'll be prompted to login to AAD with your account. This is necessary. Then it will error out saying you don't have permissions. The last part of the URL will include the format of `#<your_aad_id>`. This will be needed shortly.

Now you need to add your AAD user as an admin in the cluster. First get the IP address of your Kubernetes cluster:

```
$ az network public-ip list --query "[*].{name:name,resourceGroup:resourceGroup,fqdn:dnsSettings.fqdn,ip:ipAddress}" -o table
```

Run the following command to add yourself as a cluster admin:

```
$ ssh <your_ssh_user_name>@<cluster_dns_name> -- kubectl create clusterrolebinding aad-default-cluster-admin-binding \
        --clusterrole=cluster-admin \
        --user 'https://sts.windows.net/<tenant-id>/#<user-id>'
```

Now your AAD user should be set as a cluster admin. Verify this by running:

```
$ kubectl get po --all-namespaces
```

This should be successful.

### Use jsonpath to extract fields from a list of output

Loop through with jsonpath with `{range}` and `{end}`

```
$ kubectl get no -o jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.spec.podCIDR}{"\n"}{end}'
```
