# Index

- [Build Fluent Bit on CentOS](#build-fluent-bit-on-centos)
- [Build Fluent Bit on CentOS with systemd support](#build-fluent-bit-on-centos-with-systemd-support)
- [Create a new output plugin for Fluent Bit](#create-a-new-output-plugin-for-fluent-bit)

### Build Fluent Bit on CentOS

```
# yum install -y git cmake gcc gcc-c++ kernel-devel
$ git clone https://github.com/fluent/fluent-bit.git
$ cd fluent-bit/build
$ cmake ..
$ make
```

To test out the installation:

```
$ bin/fluent-bit -i cpu -o stdout
```

### Build Fluent Bit on CentOS with systemd support

```
# yum install -y git cmake gcc gcc-c++ kernel-devel systemd-devel
$ git clone https://github.com/fluent/fluent-bit.git
$ cd fluent-bit/build
$ cmake ..
$ make
```

To verify that systemd is a supported input plugin:

```
$ bin/fluent-bit -h | grep systemd
```

### Create a new output plugin for Fluent Bit

1. Create the new plugin (e.g. copy the out_stdout plugin dir and alter appropriately)
1. Add the new option to the root `CMakeLists.txt`
1. Add the set to the same file for the new plugin
1. Call `REGISTER_OUT_PLUGIN` in `plugins/CMakeLists.txt` for the new plugin
1. Build: `$ cd build && cmake .. && make`
1. Verify the plugin is registered: `$ bin/fluent-bit --help | grep <new_plugin_name>`
