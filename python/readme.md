# Index

- [Install and activate a virtual environment](#install-and-activate-a-virtual-environment)
- [Create a basic Flask application](#create-a-basic-flask-application)
- [Docker configuration for a Flask application](#docker-configuration-for-a-flask-application)
- [Debugging a Flask application from the command line](#debugging-a-flask-application-from-the-command-line)
- [Use Google authentication for a Flask application](#use-google-authentication-for-a-flask-application)
- [Debug a Python or Flask application in a container with Docker Compose](#debug-a-python-or-flask-application-in-a-container-with-docker-compose)
- [Flask application logging with Gunicorn](#flask-application-logging-with-gunicorn)
- [Log to systemd journal](#log-to-systemd-journal)
- [Specify a different Pipfile with pipenv](#specify-a-different-pipefile-with-pipenv)
- [Setup pipenv virtual environment](#setup-pipenv-virtual-environment)
- [Create a pipenv script](#create-a-pipenv-script)

### Install and activate a virtual environment

```
$ python3 -m venv venv
$ . venv/bin/activate
```

### Create a basic Flask application

Install dependencies: `pip install flask gunicorn`

In main application application module (i.e. `app.py`):

```python
from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/')
def default_route():
    return jsonify('hello world')

if __name__ == '__main__':
    app.run()
```

Then to run this with gunicorn (e.g. through a Docker container):

`$ gunicorn --workers=2 --bind=0.0.0.0:8000 app:app`

### Docker configuration for a Flask application

```
FROM python:alpine3.6
WORKDIR /usr/src/app
EXPOSE 8000

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["gunicorn", "--workers=2", "--bind=0.0.0.0:8000", "app:app"]
```

`.dockerignore` should include `venv/`

### Debugging a Flask application from the command line

If the app is running with gunicorn, do *not* use gunicorn when debugging.

Run the Flask application directly: `$ python -m pdb app.py`

This will break into the debugger for the common debugging experience with pdb.

### Use Google authentication for a Flask application

1. Create a project in the Google Developer Console
1. Create an OAuth credential
1. Download the `client_secret.json` file and place it in the root of the project
1. Enable the People API
1. Use the below code as a guide

```python
mport os
import flask
import requests

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

# This variable specifies the name of a file that contains the OAuth 2.0
# information for this application, including its client_id and client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
# SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly']
SCOPES = ['https://www.googleapis.com/auth/userinfo.profile']
API_SERVICE_NAME = 'people'
API_VERSION = 'v1'

app = flask.Flask(__name__)
# Note: A secret key is included in the sample so that it works.
# If you use this code in your application, replace this with a truly secret
# key. See http://flask.pocoo.org/docs/0.12/quickstart/#sessions.
app.secret_key = 'REPLACE ME - this value is here as a placeholder.'

# this should only be set for development
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

@app.route('/')
def index():
    return print_index_table()


@app.route('/test')
def test_api_request():
    if 'credentials' not in flask.session:
        return flask.redirect('authorize')

    # Load credentials from the session.
    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

#  drive = googleapiclient.discovery.build(
#      API_SERVICE_NAME, API_VERSION, credentials=credentials)

    people = googleapiclient.discovery.build(
        API_SERVICE_NAME,
        API_VERSION,
        credentials=credentials
    )

    me = people.people().get(
        resourceName='people/me',
        personFields='names,emailAddresses'
    ).execute()

#  files = drive.files().list().execute()

    # Save credentials back to session in case access token was refreshed.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    flask.session['credentials'] = credentials_to_dict(credentials)

    return flask.jsonify(message='hello world', me=me, credentials=credentials_to_dict(credentials))


@app.route('/authorize')
def authorize():
    # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)

    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True)

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    flask.session['credentials'] = credentials_to_dict(credentials)

    return flask.redirect(flask.url_for('test_api_request'))


@app.route('/revoke')
def revoke():
    if 'credentials' not in flask.session:
        return ('You need to <a href="/authorize">authorize</a> before ' +
                'testing the code to revoke credentials.')

    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

    revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',
        params={'token': credentials.token},
        headers = {'content-type': 'application/x-www-form-urlencoded'})

    status_code = getattr(revoke, 'status_code')
    if status_code == 200:
        return('Credentials successfully revoked.' + print_index_table())
    else:
        return('An error occurred.' + print_index_table())


@app.route('/clear')
def clear_credentials():
    if 'credentials' in flask.session:
        del flask.session['credentials']
    return ('Credentials have been cleared.<br><br>' +
          print_index_table())


def credentials_to_dict(credentials):
    return {'token': credentials.token,
          'refresh_token': credentials.refresh_token,
          'token_uri': credentials.token_uri,
          'client_id': credentials.client_id,
          'client_secret': credentials.client_secret,
          'scopes': credentials.scopes}

def print_index_table():
    return ('<table>' +
          '<tr><td><a href="/test">Test an API request</a></td>' +
          '<td>Submit an API request and see a formatted JSON response. ' +
          '    Go through the authorization flow if there are no stored ' +
          '    credentials for the user.</td></tr>' +
          '<tr><td><a href="/authorize">Test the auth flow directly</a></td>' +
          '<td>Go directly to the authorization flow. If there are stored ' +
          '    credentials, you still might not be prompted to reauthorize ' +
          '    the application.</td></tr>' +
          '<tr><td><a href="/revoke">Revoke current credentials</a></td>' +
          '<td>Revoke the access token associated with the current user ' +
          '    session. After revoking credentials, if you go to the test ' +
          '    page, you should see an <code>invalid_grant</code> error.' +
          '</td></tr>' +
          '<tr><td><a href="/clear">Clear Flask session credentials</a></td>' +
          '<td>Clear the access token currently stored in the user session. ' +
          '    After clearing the token, if you <a href="/test">test the ' +
          '    API request</a> again, you should go back to the auth flow.' +
          '</td></tr></table>')


if __name__ == '__main__':
    # When running locally, disable OAuthlib's HTTPs verification.
    # ACTION ITEM for developers:
    #     When running in production *do not* leave this option enabled.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    # Specify a hostname and port that are set as a valid redirect URI
    # for your API project in the Google API Console.
    app.run('localhost', 8000, debug=True)
```

### Debug a Python or Flask application in a container with Docker Compose

The scenario is one of the following:
- Need to attach the debugger to a running process in a container that requires communication with other containers via Docker Compose
- Need to attach the debugger to a Python Flask application that's running in docker-compose

The `docker-compose.yml` should appear similar to this for services that depend on the container/service that you want to debug (e.g. redis needs to be running when debugging svc1):

```
version: '3'
services:
  svc1:
    build: .
    ports:
      - "8000:8000"
    links:
      - redis
  redis:
    image: redis:alpine

```

If you want to debug `svc1` (whether a Flask application or just a vanilla Python application), do the following:

```
$ docker-compose run -p 8000:8000 svc1 python3 -m pdb app.py
```

Note: you may need to run `docker-compose build` if changes were made to the files since last build

Where `app.py` is the entry point to your application that you want to debug.

### Flask application logging with Gunicorn

When running a Flask application through Gunicorn (usually always except when needing to step through the debugger), you can set the `gunicorn.error` logger handler(s) for the Flask app logger handlers:

```python
app = Flask(__name__)

# if __name__ is '__main__' then this module is being invoked directly (outside of gunicorn)
# but if it is not then we want to use gunicorn's logger
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers

    # set the Flask app logger level to gunicorn's logger level
    # so that the gunicorn log level is taken into account
    app.logger.setLevel(gunicorn_logger.level)
```

When invoking the `gunicorn` bin, specify `--log-level=<log_level>` where `log_level` is one of the following:

- debug
- info
- warning
- error
- critical

### Log to systemd journal

Requirements:
- (RPM-based distros) `systemd-devel`
- (Deb-based distros) `build-essential`, `libsystemd-journal-dev`, `libsystemd-daemon-dev`, `libsystemd-dev`
- `pip install systemd`

```python
import logging
from systemd.journal import JournaldLogHandler

logger = logging.getLogger(__name__)
journald_handler = JournaldLogHandler()
journald_handler.setFormatter(logging.Formatter(
    '[%(levelname)s] %(message)s'
))
logger.addHandler(journald_handler)
logger.setLevel(logging.DEBUG)
```

### Specify a different Pipfile with pipenv

Use the `PIPENV_PIPFILE` env var to set a different Pipfile to use:

```
$ PIPENV_PIPFILE="./different/path/Pipfile.dev" pipenv run ...
```

### Setup pipenv virtual environment

1. Install pipenv: `$ pip install --user --upgrade pipenv`
1. Create the virtual env: `$ pipenv --three`
1. Install prod dependencies: `$ pipenv install <dep_name>`
1. Install dev dependencies: `$ pipenv install <dep_name> --dev`

### Create a pipenv script

In the `Pipfile` create a `[scripts]` section:

```
[scripts]
flask_run = "bash -c \"FLASK_APP=app.py flask run\""
```

Invoke the script by running: `$ pipenv run <script_name>`
