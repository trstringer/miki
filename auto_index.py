#!/usr/bin/env python3

"""Generate the parent index for the wiki repo"""

import os
import re

def direct_dirs():
    """Retrieve immediate directories list"""

    return sorted([
        i for i in os.listdir()
        if os.path.isdir(i)
        and i[0] != '.'
        and i not in ['venv']
    ])

def readme(dir_name):
    """Retrieve the readme from a immediate directory"""

    with open(os.path.join(dir_name, 'readme.md')) as readme_file:
        lines = [i.replace('\n', '') for i in readme_file.readlines()]

    matched_lines = []

    for line in lines:
        # short circuit if we are past the index items
        if re.match(r'^###\s', line):
            break
        elif re.match(r'^\s*-\s+\[', line):
            initial_redirect = re.search(r'(#.*)\)', line)
            modified_line = line.replace(
                initial_redirect.group(1),
                '{}{}'.format(dir_name, initial_redirect.group(1))
            )
            matched_lines.append(modified_line)

    return '## {} \n\n{}'.format(dir_name, '\n'.join(matched_lines))

def index(dir_names):
    """Generate the super index of the sub groups"""

    return '# Index\n\n{}'.format('\n'.join(
        ['- [{}](#{})'.format(i, i) for i in dir_names]
    ))

def main():
    """Main code execution"""

    new_readme_content = '{}\n\n{}'.format(
        index(direct_dirs()),
        '\n\n'.join([readme(i) for i in direct_dirs()])
    )
    with open('readme.md', 'w') as readme_file:
        readme_file.writelines(new_readme_content)

if __name__ == '__main__':
    main()
