# Index

  [Connect to server with psql](#connect-to-server-with-psql)
- [Connect to different database in psql](#connect-to-different-database-in-psql)
- [Create a postgres server in azure (mikird)](#create-a-postgres-server-in-azure-mikird)

### Connect to server with psql

```
$ psql --host <host> --port <port> --username <user> --dbname <dbname>
```

### Connect to different database in psql

```
old_db_name=> \connect <new_db_name>
```

### Create a postgres server in azure (mikird)

See the reference in the [Azure section](../azure#create-a-postgres-server)
