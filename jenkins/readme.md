# Index

- [GitHub webhook](#github-webhook)
- [Configure SSL for Jenkins nginx reverse proxy (redirect to Linux)](../linux#ssl-config-for-nginx-reverse-proxy)
- [Run docker container in pipeline](#run-docker-container-in-pipeline)
- [Pass secrets to pipeline](#pass-secrets-to-pipeline)

### GitHub webhook

Follow the instructions [here](https://support.cloudbees.com/hc/en-us/articles/224543927-GitHub-webhook-configuration)

### Run docker container in pipeline

Sample Jenkinsfile:

```
pipeline {
  agent any
  environment {
    BUILD_VERSION = '$(python3 -c \'from version import VERSION; print(VERSION,end="")\')'
  }
  stages {
    stage('Build') {
      steps {
        echo "building..."
        sh "docker build -t trstringer/jenkinstest:${env.BUILD_VERSION} ."
        sh "docker run -d -p 8000:8000 --rm --name jenkinstest${env.BUILD_VERSION} trstringer/jenkinstest:${env.BUILD_VERSION}"
      }
    }
    stage('Test') {
      steps {
        sh 'bash ./integration/web_response.sh'
      }
    }
    stage('Deliver') {
      when {
        expression {
          currentBuild.result == null || currentBuild.result == 'SUCCESS'
        }
      }
      steps {
        echo "pushing image to container registry..."
        sh "docker push trstringer/jenkinstest:${env.BUILD_VERSION}"
      }
    }
  }
  post {
    always {
      sh "docker stop jenkinstest${env.BUILD_VERSION}"
      sh "docker image prune -f"
    }
    failure {
      mail bcc: '', body: 'The build has failed. Look into it.', cc: '', from: '', replyTo: '', subject: 'Build failed', to: 'github@trstringer.com'
    }
  }
}
```

This assumes a few things:

1. The version for the image can be dynamically obtained (example uses python and a version.py module)
1. The Jenkins machine is logged into a docker registry (i.e. docker hub)

### Pass secrets to pipeline

If you want to test an application that requires secrets, you should **not** put these secrets in the `Jenkinsfile` because it shouldn't be committed to version control.

To securely store and reference secrets from a declarative pipeline:

Create the credentials in the current job folder.

Then in the `Jenkinsfile` you should reference it with the `credentials()` function:

```
pipeline {
    environment {
        SOME_SECRET = credentials('my_secret_id')
    }
}
```

This does not have to be stored in an environment variable, `credentials()` can be called anywhere in the pipeline.
